﻿using UnityEngine;
using System.Collections;

public class ScreenLaunchFromPoint : MonoBehaviour {

    public Transform _headOriginePoint;

    public float _maxPowerScreenPourcent = 0.7f;

    public bool _isPressing;

    public Transform _debugCube;
    
    public Vector2 _upPosition;


    public delegate void OnHeadDirectionGiven(float angleInDegree, float forcePourcent);
    public OnHeadDirectionGiven onScreenRelease;

    void Update()
    {
        bool wasPressing = _isPressing;
        _isPressing = Input.touchCount > 0;


        if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1) || (_isPressing && ! wasPressing))
        {
            StartPressingOfScreenFocusDetected();
        }

        if (Input.GetMouseButtonUp(0) || Input.GetMouseButtonUp(1) || (!_isPressing && wasPressing)) {
            ReleaseOfScreenFocusDetected();
        }


    }

    void StartPressingOfScreenFocusDetected() {
    }

    void ReleaseOfScreenFocusDetected()
    {

        float angleOfLaunch = 45f;
        float forceLaunch = 1f;
        _upPosition = GetScreenPosition();

        Debug.Log("Screen Position on Release:" + _upPosition);

        angleOfLaunch = Vector3.Angle(_upPosition, new Vector3(1,0,0) );

        float distanceBetweenHeadAndFocus = Vector3.Distance(Vector3.zero, _upPosition);

        forceLaunch = Mathf.Clamp((distanceBetweenHeadAndFocus / (_maxPowerScreenPourcent)), 0f, 1f);
        Debug.Log("Release Launch:" + angleOfLaunch + " - " + forceLaunch);

        onScreenRelease(angleOfLaunch, forceLaunch);
    }

    private static Vector3 GetScreenPosition()
    {
        Vector3 screenPosition = Input.mousePosition;
        if (Input.touchCount > 0)
            screenPosition = Input.GetTouch(0).position;

        screenPosition.y /= Screen.height;
        screenPosition.x /= Screen.width;

        return screenPosition;
    }
}
