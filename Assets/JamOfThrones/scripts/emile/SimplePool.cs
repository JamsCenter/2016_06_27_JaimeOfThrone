﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// Made by Emile Sonneveld
// This work is licensed under a Creative Commons Attribution 4.0 International License.

public class SimplePool : MonoBehaviour
{

    public GameObject prefab;
    public int preLoad = 10;
    public bool canGrow = true;
    public bool parentInstances = true;

    readonly List<GameObject> objectPool = new List<GameObject>();
    int cursor = 0; // remember where the last inactive object was found

    public void PreLoadObjects(int count)
    {
        // objectPool.Capacity += count;   can be better
        for (int i = 0; i < count; ++i)
        {
            var inst = (GameObject)Instantiate(prefab);
            inst.SetActive(false);
            if (parentInstances)
                inst.transform.parent = gameObject.transform;
            objectPool.Add(inst);
        }
    }


    public GameObject Spawn(Vector3 position)
    {
        var count = objectPool.Count;
        for (int i = cursor; i < count + cursor; ++i)
        {
            if (objectPool[i%count].activeInHierarchy) continue;
            cursor = i % count;
            var inst = objectPool[cursor];
            inst.transform.position = position;
            inst.SetActive(true);

            inst.BroadcastMessage(
                "OnSpawned",
                this,
                SendMessageOptions.DontRequireReceiver
                );
            return inst;
        }

        if (canGrow)
        {
            var inst = (GameObject)Instantiate(prefab);
            inst.transform.position = position;
            if (parentInstances)
                inst.transform.parent = gameObject.transform;
            objectPool.Add(inst);

            inst.BroadcastMessage(
                "OnSpawned",
                this,
                SendMessageOptions.DontRequireReceiver
            );
            return inst;
        }
        else
        {
            return null;
        }
    }

    public void Despawn(GameObject inst)
    {
        if (!objectPool.Contains(inst))
        {
            Debug.LogWarning("you tried to despawn an object wich wasn't spawned with this pool");
        }

        inst.SetActive(false);
        inst.BroadcastMessage(
            "OnDespawned",
            this,
            SendMessageOptions.DontRequireReceiver
        );
    }

    public void DespawnAll()
    {
        foreach (var inst in objectPool)
        {
            inst.SetActive(false);
            inst.BroadcastMessage(
                "OnDespawned",
                this,
                SendMessageOptions.DontRequireReceiver
                );
        }
    }

    void SeekExistingLevelObjects()
    {
    }

    public static SimplePool instance;
    void Awake()
    {
        instance = this;
        PreLoadObjects(preLoad);
    }

    void Reset()
    {
    }

}