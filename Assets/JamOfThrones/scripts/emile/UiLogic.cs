﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class UiLogic : MonoBehaviour
{
    public GameObject endScreen;
    public GameObject panelWin;
    public GameObject panelLoose;
    public GameObject panelPerfect;
    public static UiLogic instance;
    

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        ResetUi();
    }

    void ResetUi()
    {
        endScreen.SetActive(false);
        panelWin.SetActive(false);
        panelLoose.SetActive(false);
        panelPerfect.SetActive(false);
    }

    void Update()
    {

    }

    public void ShowScreenLoose()
    {
        if (endScreen.activeSelf)
            return;
        ResetUi();
        endScreen.SetActive(true);
        panelLoose.SetActive(true);
    }
    public void ShowScreenWin()
    {
        if (endScreen.activeSelf)
            return;
        ResetUi();
        endScreen.SetActive(true);
        panelWin.SetActive(true);
    }
    public void ShowScreenPerfect()
    {
        if (endScreen.activeSelf)
            return;
        ResetUi();
        endScreen.SetActive(true);
        panelPerfect.SetActive(true);
    }

    public void UiRestart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex); // TODO: use current scene
    }

    public void UiExit()
    {
        Application.Quit();
    }
}
