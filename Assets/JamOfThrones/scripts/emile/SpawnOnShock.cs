﻿using UnityEngine;
using System.Collections;

public class SpawnOnShock : MonoBehaviour
{
    SimplePool pool;
    private Vector2 prevVel = new Vector2();
    private Rigidbody2D rb;

    private void Start()
    {
        pool = SimplePool.instance;
        rb = GetComponent<Rigidbody2D>();
        prevVel = rb.velocity;
    }

    private void Update()
    {
        if ((prevVel - rb.velocity).sqrMagnitude > 4 * 4)
        {
            pool.Spawn(transform.position);
        }

        prevVel = rb.velocity;
    }
}
